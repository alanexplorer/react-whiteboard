import './App.css';
// import Poly from './draw/polygon';
import Whiteboard from './draw/whiteboard';
import HomePage from './components/HomePage'

function App() {
  return (
    <div className="App">
      <header className="App-header">
	  {/*<Whiteboard/>*/}
        {/* <Poly/> */}
	  <HomePage/>
      </header>
    </div>
  );
}

export default App;
