var konva,polyline,mouseDown=false,pts=[],lastPt=0,polyType='false',polyBtn,poly=false,bgColor,mouse;

$( document ).ready(function() {
  konva = $('#konva');
  var stage = new Konva.Stage({container: 'konva',width: 600,height: 480});
  var layer = new Konva.Layer(); stage.add(layer);

konva.on('mousedown', function (e) {
  mouse = stage.getPointerPosition();
  if (poly == true) {
    if (pts.length > 1) { pts.splice(-2,2); } //remove the one of the duplicate start and end pts.
    pts.push(mouse.x, mouse.y);
    polyline = new Konva.Line({points:pts,name:'temp',fill:bgColor,stroke:'black',strokeWidth:1,draggable:false});
    lastPt=(pts.length); mouseDown=true; layer.add(polyline);
    $('#debug').text(pts);
  }
});

konva.on('mousemove', function(e){
  if (poly == true && mouseDown==true) {
    mouse = stage.getPointerPosition();
    polyline.points()[lastPt] = mouse.x;
    polyline.points()[lastPt+1] = mouse.y;
    layer.draw();
  };
});

konva.on('dblclick', function (e) {
  if (polyBtn) {
    
    tr = layer.find('.temp'); tr.destroy(); 
    var polyObj = new Konva.Line({points:pts,name:'poly',fill:bgColor,stroke:'black',strokeWidth:1,draggable:true,closed:polyType,hitStrokeWidth:10});
    layer.add(polyObj); layer.draw();
    poly=false; polyBtn=''; lastPt=0; mouseDown=false; pts=[];
    $('.toolBarTable td').removeClass('active');
  };
}); //end dblclick

stage.on('click tap', function(e) {
  stage.find('Transformer').destroy();
  if (!e.target.hasName('poly')) { layer.draw(); return; }
  var tr = new Konva.Transformer();
  layer.add(tr); tr.attachTo(e.target); layer.batchDraw();
});

////////////////////// HTML ////////////////////////////////////////
  $('.toolBarTable tr:first td').on('click', function(e) {
    $('.toolBarTable td').removeClass('active');
    $(this).addClass('active'); polyBtn=$(this).attr('id');
    if (polyBtn=='polyline') { polyType=false; poly=true; bgColor=''; }
    if (polyBtn=='polygon') { polyType=true; poly=true; bgColor='green'; }
  });

}); //end ready