import { useEffect, useState } from "react";
import Konva from "konva";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import mapShape from '../img/simple_factory.png'

const getMousePos = stage => {
    return [stage.getPointerPosition().x, stage.getPointerPosition().y];
};

const Poly = () => {

    const defaultPoly = {
        curMousePos: [0.0, 0.0],
        isFinished: false,
    }
    
    const [poly, setPoly] = useState(defaultPoly)
    const [points, setPoints] = useState([])

    useEffect(() => {
        draw()
    })

    const handleClick = event => {

        if (poly.isFinished) return;

        const stage = event.target.getStage();
        const mousePos = getMousePos(stage);
        setPoints([...points, mousePos])
    };

    const handleMouseMove = event => {
        const stage = event.target.getStage();
        const mousePos = getMousePos(stage);
        setPoly({...poly, curMousePos : mousePos})
    };

    const handleDClick = event => {
        if(points.length >= 3){
            setPoly({...poly, isFinished : true}) 
        }
    };


    const draw = () => {
        var stage = new Konva.Stage({
            container: 'amr',
            width: 1280,
            height: 1024
        });

        var layer = new Konva.Layer();
        stage.add(layer)        

        var imageObj = new window.Image();
        imageObj.src = mapShape

  
        var map = new Konva.Image({
            x: 0,
            y: 0,
            image: imageObj,
            width: 1280,
            height: 1024
        });
        
        layer.add(map)

        let flattenedPoints = points
        .concat(poly.isFinished ? [] : poly.curMousePos)
        .reduce((a, b) => a.concat(b), []);
        
        var polygon = new Konva.Line({
            points: Object.values(flattenedPoints),
            stroke: "black",
            strokeWidth: 2,
            fill: '#FF5733',
            closed: poly.isFinished,
            opacity: 0.4
        });

        layer.add(polygon)

        stage.on('mousedown', handleClick)
        stage.on('mousemove', handleMouseMove)
        stage.on('dblclick', handleDClick)
    }

    return(
        <Row>
            <Col>
                <div id="amr"></div>
            </Col>
            <Col>
                <Row>
                    cursor_x: {poly.curMousePos[0].toFixed(2)}<br/>
                    cursor_y: {poly.curMousePos[1].toFixed(2)}
                </Row>
                <hr/>
                <Row>
                    <FormGroup>
                        <Label for="area-select"> Area Selection</Label>
                        <Input type="select" name="selectArea" id="area">
                            <option>Preferred zones</option>
                            <option>Unpreferred zones</option>
                            <option>Forbidden zones</option>
                            <option>Critical zones</option>
                            <option>Speed zones</option>
                            <option>Sound and light zones</option>
                            <option>Planner zones</option>
                            <option>I/O module zones</option>
                        </Input>
                    </FormGroup>
                </Row>
            </Col>
        </Row>
    )
}

export default Poly;