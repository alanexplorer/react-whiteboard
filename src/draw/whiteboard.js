import React, { Component } from "react";
import { Stage, Layer, Line, Rect } from "react-konva";
    
class Whiteboard extends Component {
    
    state = {
        points: [],
        curMousePos: [0, 0],
        isMouseOverStartPoint: false,
        isFinished: false
    };

    getMousePos = stage => {
        return [stage.getPointerPosition().x, stage.getPointerPosition().y];
    };

    handleClick = event => {
        const {
            state: { points, isMouseOverStartPoint, isFinished },
            getMousePos
        } = this;

        const stage = event.target.getStage();
        const mousePos = getMousePos(stage);

        if (isFinished) {
            return;
        }
        if (isMouseOverStartPoint && points.length >= 3) {
            this.setState({
            isFinished: true
            });
        } else {
            this.setState({
            points: [...points, mousePos]
            });
        }
    };

    handleMouseMove = event => {
        const { getMousePos } = this;
        const stage = event.target.getStage();
        const mousePos = getMousePos(stage);

        this.setState({
            curMousePos: mousePos
        });
    };

    handleMouseOverStartPoint = event => {
        if (this.state.isFinished || this.state.points.length < 3) return;
        event.target.scale({ x: 2, y: 2 });
        this.setState({
            isMouseOverStartPoint: true
        });
    };
    handleMouseOutStartPoint = event => {
        event.target.scale({ x: 1, y: 1 });
        this.setState({
            isMouseOverStartPoint: false
        });
    };

    handleDragMovePoint = event => {
        const points = this.state.points;
        const index = event.target.index - 1;
        console.log(event.target);
        const pos = [event.target.attrs.x, event.target.attrs.y];
        console.log("move", event);
        console.log(pos);
        this.setState({
            points: [...points.slice(0, index), pos, ...points.slice(index + 1)]
        });
    };

    render() {

        const flattenedPoints = this.state.points
            .concat(this.state.isFinished ? [] : this.state.curMousePos)
            .reduce((a, b) => a.concat(b), []);
        return (
            <Stage
            width={window.innerWidth}
            height={window.innerHeight}
            onMouseDown={this.handleClick}
            onMouseMove={this.handleMouseMove}
            >
            <Layer>
                <Line
                points={flattenedPoints}
                stroke={this.state.isFinished ? "white" : "black"}
                strokeWidth={2}
                closed={this.state.isFinished}
                fill="red"
                opacity={this.state.isFinished ? 0.4 : 1.0}
                dash = {this.state.isFinished? [0, 0] : [10, 10]}
                />
                {this.state.points.map((point, index) => {
                const width = 4;
                const x = point[0] - width / 2;
                const y = point[1] - width / 2;
                const startPointAttr =
                    index === 0
                    ? {
                        hitStrokeWidth: 12,
                        onMouseOver: this.handleMouseOverStartPoint,
                        onMouseOut: this.handleMouseOutStartPoint
                        }
                    : null;
                return (
                    <Rect
                    visible={!this.state.isFinished}
                    key={index}
                    x={x}
                    y={y}
                    width={width}
                    height={width}
                    fill="white"
                    stroke="black"
                    strokeWidth={3}
                    onDragMove={this.handleDragMovePoint}
                    draggable
                    {...startPointAttr}
                    />
                );
                })}
            </Layer>
            </Stage>
        );
    }
}

export default Whiteboard;