import Konva from "konva";

export const addPolygon = (stage, layer) => {

	let isFinished = false;
	let curMousePos = [];
	let flattenedPoints = [];
	let points = []

	var polygon = new Konva.Line({
		points: Object.values(flattenedPoints),
		stroke: "black",
		strokeWidth: 2,
		fill: '#FF5733',
		closed: isFinished,
		opacity: 0.4
	});

	layer.add(polygon)

	const getMousePos = stage => {
		return [stage.getPointerPosition().x, stage.getPointerPosition().y];
	};	

	stage.on("mousedown touchstart", function(e) {

		if (isFinished) return;

        stage = e.target.getStage();
        curMousePos = getMousePos(stage);
		points = [...points, curMousePos]

		flattenedPoints = points
        .concat(isFinished ? [] : curMousePos)
        .reduce((a, b) => a.concat(b), []);

		layer.batchDraw()

	});

	stage.on('mousemove', function(e){

		stage = e.target.getStage();
        curMousePos = getMousePos(stage);
		console.log(flattenedPoints)
	});

	stage.on('dblclick', function (e) {
		
		if(points.length >= 6){
			isFinished = true
        }

	}); //end dblclick
};
