import Konva from "konva";

export const addLine = (stage, layer) => {
	let isFinished = false;
	let lastLine;


	let pos = stage.getRelativePointerPosition();
	console.log('stage = ', stage)
	console.log('pos = ', pos)
	lastLine = new Konva.Line({
			stroke: "teal",
			strokeWidth: 2,
			globalCompositeOperation: "source-over",
			points: [],
			fill: "magenta",
			closed: false,
			opacity: 1,
			dash: [10, 10]
		});

	layer.add(lastLine);
	stage.on("mousedown touchstart", function(e) {
		console.warn('mousedown = ', lastLine.points())

		if(isFinished) {
			return;
		}

		const pos = stage.getRelativePointerPosition();
			let newPoints = lastLine.points().concat([pos.x, pos.y]);
			lastLine.points(newPoints);
			layer.batchDraw();


	});

	stage.on("mouseup", function(e) {
		console.warn('mouseup = ', lastLine.points())

	});

	stage.on("dblclick", function() {
		console.warn('dbclick = ')
		if(lastLine.points().length >= 6)
			{
				isFinished = true;
				lastLine.setAttr('closed', isFinished)
				lastLine.setAttr('opacity', 0.4)
				lastLine.setAttr('dash', [0.0, 0.0])
				lastLine.setAttr('strokeWidth', 0)
			}




	});



};






















